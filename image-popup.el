;;; image-popup.el --- Resizes images to line hight and can display them in a popup  -*- lexical-binding: t; -*-

;; Copyright (C) 2022

;; Author:  Marco Pawłowski
;; Keywords: convenience
;; Version: 0.5
;; Package-Requires: ((emacs "27.1"))
;; URL: https://gitlab.com/OlMon/image-popup

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package resizes all images in a buffer to a line hight size.
;; The images can then be displayed in small popup.  This prevents to bufer jumping caused
;; by images.

;;; Code:
(defgroup image-popup nil
  "Image popup - Moves images from a buffer to a frame."
  :group 'convenience
  :group 'image-popup
  :prefix "image-popup-")

(defvar image-popup--image-frame nil
  "The frame with the image inside.")
(defvar image-popup--buffer-name " *image-popup*"
  "The name of the buffer with the image inside.")

(defvar image-popup-max-height 1.0
  "Factor of the maximal allowed inline image.
Everything bigger will be scaled down.
The factor is relative to the `default-line-height'.")

(defvar image-popup-x-offset 10
  "The horizontal offset to frame from the pointer.")
(defvar image-popup-y-offset 10
  "The vertical offset to frame from the pointer.")
(defvar image-popup-max-height 0.7
  "The maximal height of the image frame.
Can be in percentage of the buffer size or total pixel.")
(defvar image-popup-max-width 0.7
  "The maximal width of the image frame.
Can be in percentage of the buffer size or total pixel.")

(defvar image-popup-map
  (let ((map (make-sparse-keymap)))
    (define-key map "n" #'image-popup-display-next-image)
    (define-key map "p" #'image-popup-display-previous-image)
    (define-key map [t] #'image-popup-popup-hide)
    map)
  "Image-popup keymap used when popup is shown.")


;; Images
(defun image-popup--find-images-in-buffer (&optional start end rev)
  "Find all images displayed in the current buffer.
Starting from START to END reverse the result if REV is non-nil."
  (save-excursion
    (let ((imgs (list)))
      (goto-char (or start (point-min)))
      (while (not (or (and end (= (point) end)) (eobp)))
        ;; TODO: Change to (image-at-point-p) when emcas hits 29.1
        (when (condition-case nil
                  (prog1 t (image--get-image))
                (error nil))
          (unless (eq (image--get-image) (cdar imgs))
            (push (cons (point) (image--get-image)) imgs)))
        (forward-char))
      (sort imgs (lambda (x y) (when (< (car x) (car y)) x)))
      (if rev
          (reverse imgs)
        imgs))))

(defun image-popup--rescale-all-images ()
  "Rescale all images in the current buffer."
  (let ((imgs (image-popup--find-images-in-buffer)))
    (while imgs
      (let ((img (car imgs)))
        (image-popup--rescale-image (car img)))
      (setq imgs (cdr imgs)))))

(defun image-popup--rescale-image (pos)
  "Scale image at POS."
  (let ((img-height (cdr (image-size (image--get-image pos) t)))
        (default-height (default-line-height)))
    (when (> img-height (* default-height image-popup-max-height))
      (image--change-size (/ (* default-height image-popup-max-height) img-height) pos))))


;; Finding and displaying images
(defun image-popup--find-previous-image ()
  "Find the previous image searching from point."
  (let ((markers (image-popup--find-images-in-buffer nil (point) t)))
    (when markers
      ;; TODO: Change to (image-at-point-p) when emcas hits 29.1
      (if (condition-case nil
              (prog1 t (image--get-image))
            (error nil))
          (while (and markers (eq (image--get-image) (cdar markers)))
            (setq markers (cdr markers))))
      (when markers
        (goto-char (caar markers))))))

(defun image-popup--find-next-image ()
  "Find the next marker searching from point."
  (let ((markers (image-popup--find-images-in-buffer (point))))
    (when markers
      ;; TODO: Change to (image-at-point-p) when emcas hits 29.1
      (if (condition-case nil
              (prog1 t (image--get-image))
            (error nil))
          (while (and markers (eq (image--get-image) (cdar markers)))
            (setq markers (cdr markers))))
      (when markers
        (goto-char (caar markers))))))

(defun image-popup-display-image-at-point (&optional point)
  "Display the image at POINT."
  (interactive)
  (let* ((point (or point (point)))
         (markers (image-popup--find-images-in-buffer point)))
    (when markers
      (when (equal (point)  (caar markers))
        (image-popup--display (cdar markers) )))))

(defun image-popup-display-next-image ()
  "Display the next image from point onwards."
  (interactive)
  (image-popup--find-next-image)
  (recenter-top-bottom '(4))
  (image-popup-display-image-at-point))

(defun image-popup-display-previous-image ()
  "Display the previous image from point backwards."
  (interactive)
  (image-popup--find-previous-image)
  (recenter-top-bottom '(4))
  (image-popup-display-image-at-point))

(defun image-popup--height-percentage-to-pixel-size (percent)
  "Calculate the percentage PERCENT buffer height to total pixel."
  (truncate (* (window-pixel-height) percent)))

(defun image-popup--width-percentage-to-pixel-size (percent)
  "Calculate the percentage PERCENT buffer width to total pixel."
  (truncate (* (window-pixel-width) percent)))

(defun image-popup--get-max-image-size ()
  "Calculate the max image size."
  (let ((hsize (if (floatp image-popup-max-height)
                   (+ (* 2 (abs image-popup-x-offset)) (image-popup--height-percentage-to-pixel-size image-popup-max-height))
                 image-popup-max-height))
        (vsize (if (floatp image-popup-max-width)
                   (+ (* 2 (abs image-popup-y-offset)) (image-popup--width-percentage-to-pixel-size image-popup-max-width))
                 image-popup-max-width)))
    (cons hsize vsize)))

(defun image-popup--calc-image-size (image)
  "Calculate the max image size for the IMAGE."
  (let*
      ((img-width (car (image-size image t)))
       (img-height (cdr (image-size image t)))
       (max-sizes (image-popup--get-max-image-size)))
    (when (> img-height (cdr max-sizes))
      (let ((factor (/ (float (cdr max-sizes)) img-height)))
        (setq img-width (truncate (* img-width factor)))
        (setq img-height (truncate (* img-height factor)))))
    (when (> img-width (car max-sizes))
      (let ((factor (/ (float (car max-sizes)) img-width)))
        (setq img-width (truncate (* img-width factor)))
        (setq img-height (truncate (* img-height factor)))))
    (cons img-width img-height)))

;; Work on Images direct
(defun image-popup--display (image)
  "Display the IMAGE in a popup."
  ;; TODO image scaling.....
  (set-transient-map image-popup-map)
  (let*
      ((new-image (create-image (if (plist-get (cdr image) :file)
                                    (plist-get (cdr image) :file)
                                  (plist-get (cdr image) :data))
                                nil
                                (plist-get (cdr image) :data)))
       (img-data (cdr new-image))
       (img-dims (image-popup--calc-image-size new-image))
       (x (car (posn-x-y (posn-at-point))))
       (y (cdr (posn-x-y (posn-at-point)))))
    (plist-put img-data :width (car img-dims))
    (plist-put img-data :height (cdr img-dims))
    (setq new-image (cons 'image img-data))
    (if (> (+ x (car img-dims)) (window-pixel-width))
        (setq x (- x (- (+ x (car img-dims)) (window-pixel-width)) image-popup-x-offset)))
    (if (> (+ y (cdr img-dims)) (- (window-pixel-height) (* 3 (default-line-height))))
        (setq y (- y (- (+ y (cdr img-dims)) (window-pixel-height)) (* 3 (default-line-height)) image-popup-y-offset)))
    (image-popup--make-frame (+ x image-popup-x-offset (car (window-inside-pixel-edges)))
                             (+ y image-popup-y-offset (cadr (window-inside-pixel-edges)))
                             (car img-dims) (cdr img-dims)
                             new-image)))


;; Image-Popup Frame creation, most code taken from minads corfu package
;; https://github.com/minad/corfu
(defvar image-popup--frame-parameters
  '((no-accept-focus . t)
    (no-focus-on-map . t)
    (min-width . t)
    (min-height . t)
    (width . 0)
    (height . 0)
    (border-width . 0)
    (child-frame-border-width . 1)
    (left-fringe . 0)
    (right-fringe . 0)
    (vertical-scroll-bars . nil)
    (horizontal-scroll-bars . nil)
    (menu-bar-lines . 0)
    (tool-bar-lines . 0)
    (tab-bar-lines . 0)
    (no-other-frame . t)
    (no-other-window . t)
    (no-delete-other-windows . t)
    (unsplittable . t)
    (undecorated . t)
    (cursor-type . nil)
    (visibility . nil)
    (no-special-glyphs . t)
    (desktop-dont-save . t))
  "Default child frame parameters.")

(defvar image-popup--buffer-parameters
  '((mode-line-format . nil)
    (header-line-format . nil)
    (tab-line-format . nil)
    (tab-bar-format . nil) ;; Emacs 28 tab-bar-format
    (frame-title-format . "")
    (truncate-lines . t)
    (cursor-in-non-selected-windows . nil)
    (cursor-type . nil)
    (show-trailing-whitespace . nil)
    (display-line-numbers . nil)
    (left-fringe-width . nil)
    (right-fringe-width . nil)
    (left-margin-width . 0)
    (right-margin-width . 0)
    (fringes-outside-margins . 0)
    (buffer-read-only . t))
  "Default child frame buffer parameters.")

(defvar image-popup--mouse-ignore-map
  (let ((map (make-sparse-keymap)))
    (dotimes (i 7)
      (dolist (k '(mouse down-mouse drag-mouse double-mouse triple-mouse))
        (define-key map (vector (intern (format "%s-%s" k (1+ i)))) #'ignore)))
    map)
  "Ignore all mouse clicks.")

(defun image-popup--make-buffer (content)
  "Create image-popup buffer with CONTENT."
  (let ((fr face-remapping-alist)
        (ls line-spacing)
        (buffer (get-buffer-create image-popup--buffer-name)))
    (with-current-buffer buffer
      ;;; XXX HACK install mouse ignore map
      (use-local-map image-popup--mouse-ignore-map)
      (dolist (var image-popup--buffer-parameters)
        (set (make-local-variable (car var)) (cdr var)))
      (setq-local face-remapping-alist (copy-tree fr)
                  line-spacing ls)
      ;;(cl-pushnew 'image-popup-default (alist-get 'default face-remapping-alist))
      (let ((inhibit-modification-hooks t)
            (inhibit-read-only t))
        (erase-buffer)
        (goto-char (point-min))
        (put-image content (point))
        (goto-char (point-min))))
    buffer))

;; Function adapted from posframe.el by tumashu
(defvar x-gtk-resize-child-frames) ;; Not present on non-gtk builds
(defun image-popup--make-frame (x y width height content)
  "Show child frame at X/Y with WIDTH/HEIGHT and CONTENT."
  (let* ((window-min-height 1)
         (window-min-width 1)
         (x-gtk-resize-child-frames
          (let ((case-fold-search t))
            (and
             ;; XXX HACK to fix resizing on gtk3/gnome taken from posframe.el
             ;; More information:
             ;; * https://github.com/minad/image-popup/issues/17
             ;; * https://gitlab.gnome.org/GNOME/mutter/-/issues/840
             ;; * https://lists.gnu.org/archive/html/emacs-devel/2020-02/msg00001.html
             (string-match-p "gtk3" system-configuration-features)
             (string-match-p "gnome\\|cinnamon"
                             (or (getenv "XDG_CURRENT_DESKTOP")
                                 (getenv "DESKTOP_SESSION") ""))
             'resize-mode)))
         (after-make-frame-functions)
         (border (alist-get 'child-frame-border-width image-popup--frame-parameters))
         (buffer (image-popup--make-buffer content))
         (parent (window-frame)))
    (unless (and (frame-live-p image-popup--image-frame)
                 (eq (frame-parent image-popup--image-frame) parent))
      (when image-popup--image-frame (delete-frame image-popup--image-frame))
      (setq image-popup--image-frame (make-frame
                               `((parent-frame . ,parent)
                                 (minibuffer . ,(minibuffer-window parent))
                                 ;; Set `internal-border-width' for Emacs 27
                                 (internal-border-width . ,border)
                                 ,@image-popup--frame-parameters))))
    
    (let ((win (frame-root-window image-popup--image-frame)))
      (set-window-buffer win buffer)
      ;; Mark window as dedicated to prevent frame reuse (#60)
      (set-window-dedicated-p win t))
    (set-frame-size image-popup--image-frame width height t)
    (if (frame-visible-p image-popup--image-frame)
        ;; XXX HACK Avoid flicker when frame is already visible.
        ;; Redisplay, wait for resize and then move the frame.
        (unless (equal (frame-position image-popup--image-frame) (cons x y))
          (redisplay 'force)
          (sleep-for 0.01)
          (set-frame-position image-popup--image-frame x y))
      ;; XXX HACK: Force redisplay, otherwise the popup sometimes does not
      ;; display content.
      (set-frame-position image-popup--image-frame x y)
      (redisplay 'force)
      (make-frame-visible image-popup--image-frame))
    (redirect-frame-focus image-popup--image-frame parent)))

(defun image-popup-popup-hide ()
  "Hide Image-Popup popup."
  (interactive)
  (when (frame-live-p image-popup--image-frame)
    (make-frame-invisible image-popup--image-frame)))


(defun image-popup--enable ()
  "Enable image-popup in the current buffer."
  (image-popup--rescale-all-images)
  
  (add-hook 'buffer-list-update-hook #'image-popup-popup-hide)
  (add-hook 'minibuffer-mode-hook #'image-popup-popup-hide))

(defun image-popup--disable ()
  "Disable image-popup in the current buffer."
  (image-popup-popup-hide)
  ;; TODO: display all overlays and images
  (remove-hook 'buffer-list-update-hook #'image-popup-popup-hide)
  (remove-hook 'minibuffer-mode-hook #'image-popup-popup-hide))

;; Mode creation
;;;###autoload
(define-minor-mode image-popup-mode
  "Image-Popup mode definition."
  :lighter " image-popup"
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c C-f d") 'image-popup-display-image-at-point)
            (define-key map (kbd "C-c C-f n") 'image-popup-display-next-image)
            (define-key map (kbd "C-c C-f p") 'image-popup-display-previous-image)
            (define-key map (kbd "C-c C-f q") 'image-popup-popup-hide)
            map)

  (if image-popup-mode
      (image-popup--enable)
    (image-popup--disable)))

;;;###autoload
(defun image-popup-reload ()
  "Reload image-popup.
This is needed for buffers that change, like Eww nad Nov."
  (interactive)
  (if image-popup-mode
      (progn (image-popup-popup-hide)
             (image-popup--rescale-all-images)))
  (image-popup-mode))

(provide 'image-popup)
;;; image-popup.el ends here
